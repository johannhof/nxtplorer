package pt.ul.fc.di.nxtplorer.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import lejos.nxt.ColorSensor;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button exploreButton = (Button) findViewById(R.id.explore_button);
        final Button moveButton = (Button) findViewById(R.id.move_button);
        final Button repositionButton = (Button) findViewById(R.id.reposition_button);
        final Button endButton = (Button) findViewById(R.id.end_button);
        final TextView messageView = (TextView) findViewById(R.id.messageView);
        final MapView mapView = (MapView) findViewById(R.id.map);

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            System.exit(0);
        }

        // TODO enable bluetooth and pair device with the app

        BluetoothDevice robot = null;

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            // Loop through paired devices and find the robot
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals("iia014")) {
                    robot = device;
                    break;
                }
            }
        }

        if (robot != null) {
            try {
                final BluetoothSocket sock = robot.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                sock.connect();
                final DataOutputStream dos = new DataOutputStream(sock.getOutputStream());
                final DataInputStream dis = new DataInputStream(sock.getInputStream());


                int arr[][] = new int[10][10];

                for (int x = 0; x < arr.length; x++) {
                    for (int i = 0; i < arr[x].length; i++) {
                        arr[x][i] = MapCode.UNKNOWN;
                    }
                }

                mapView.setMap(arr);
                mapView.invalidate();
                mapView.requestLayout();

                dos.writeInt(MessageCode.MAP);
                dos.writeInt(arr.length);
                for (int[] ints : arr) {
                    for (int i : ints) {
                        dos.writeInt(i);
                    }
                }

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (true) {

                            try {
                                final int instruction = dis.readInt();
                                if (instruction == MessageCode.MAP) {
                                    // receive new map and overwrite robot map
                                    final int size = dis.readInt();
                                    final int[][] map = new int[size][size];

                                    for (int x = 0; x < map.length; x++) {
                                        for (int i = 0; i < map[x].length; i++) {
                                            map[x][i] = dis.readInt();
                                        }
                                    }

                                    mapView.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            mapView.setMap(map);
                                            mapView.invalidate();
                                            mapView.requestLayout();
                                        }
                                    });

                                    Log.d(getLocalClassName(), "Received Map");
                                } else if (instruction == MessageCode.MESSAGE) {
                                    final String text = dis.readUTF();
                                    messageView.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            messageView.setText(text);
                                        }
                                    });
                                    Log.d(getLocalClassName(), "Received Message");

                                } else if (instruction == MessageCode.END) {
                                    Log.d(getLocalClassName(), "Received End");
                                    break;
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                break;
                            }

                        }
                    }
                }).start();


                exploreButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        try {
                            dos.writeInt(MessageCode.EXPLORE);
                            dos.flush();

                        } catch (IOException ioe) {
                            System.out.println(ioe.getMessage());
                        }
                    }

                });

                moveButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        mapView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                int x = (int) (10 / (mapView.getWidth() / event.getX()));
                                int y = (int) (10 / (mapView.getHeight() / event.getY()));
                                Log.d(getLocalClassName(), x + "/" + y);
                                try {
                                    dos.writeInt(MessageCode.MOVE);
                                    dos.writeInt(x);
                                    dos.writeInt(y);
                                    dos.flush();
                                } catch (IOException ioe) {
                                    System.out.println(ioe.getMessage());
                                }
                                mapView.setOnTouchListener(null);
                                return false;
                            }
                        });
                    }
                });

                repositionButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        mapView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                int x = (int) (10 / (mapView.getWidth() / event.getX()));
                                int y = (int) (10 / (mapView.getHeight() / event.getY()));
                                Log.d(getLocalClassName(), x + "/" + y);
                                try {
                                    dos.writeInt(MessageCode.REPOSITION);
                                    dos.writeInt(x);
                                    dos.writeInt(y);
                                    dos.flush();
                                } catch (IOException ioe) {
                                    System.out.println(ioe.getMessage());
                                }
                                mapView.setOnTouchListener(null);
                                return false;
                            }
                        });
                    }
                });

                endButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        try {
                            dos.writeInt(MessageCode.END);
                            dos.flush();

                            try {
                                dis.close();
                                dos.close();
                                sock.close();
                            } catch (IOException ioe) {
                                System.out.println(ioe.getMessage());
                            }
                            finish();

                        } catch (IOException ioe) {
                            System.out.println(ioe.getMessage());
                        }
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
