package pt.ul.fc.di.nxtplorer.app;

public class MessageCode {
    public static final int EXPLORE = 1;
    public static final int STOP = 2;
    public static final int END = 3;
    public static final int MAP = 4;
    public static final int MESSAGE = 5;
    public static final int REPOSITION = 6;
    public static final int MOVE = 7;
}
