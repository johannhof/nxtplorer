package pt.ul.fc.di.nxtplorer.app;

public class MapCode {
    public static final int UNKNOWN = 0;
    public static final int DISCOVERED = 1;
    public static final int BLOCKED = 2;
    public static final int MY_POSITION = 3;
    public static final int TARGET = 4;
    public static final int DESTINATION = 5;
}
