package pt.ul.fc.di.nxtplorer.app;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class MapView extends View {

    private int[][] map;
    private Paint discovered = new Paint();
    private Paint unknown = new Paint();
    private Paint blocked = new Paint();
    private Paint myPosition = new Paint();
    private Paint target = new Paint();
    private Paint destination = new Paint();

    public MapView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MapView(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (map == null) {
            return;
        }
        discovered.setARGB(255, 100, 225, 100);
        unknown.setARGB(255, 255, 255, 255);
        blocked.setARGB(255, 0, 0, 0);
        myPosition.setARGB(255, 0, 0, 255);
        target.setARGB(255, 0, 255, 255);
        destination.setARGB(255, 255, 0, 0);

        int h = canvas.getHeight() / map.length;
        int w = canvas.getWidth() / map.length;

        for (int x = 0; x < map.length; x++) {
            for (int i = 0; i < map[x].length; i++) {
                if (map[x][i] == MapCode.UNKNOWN) {
                    canvas.drawCircle(w * x + w / 2, h * i + h / 2, w / 2, unknown);
                } else if (map[x][i] == MapCode.DISCOVERED) {
                    canvas.drawCircle(w * x + w / 2, h * i + h / 2, w / 2, discovered);
                } else if (map[x][i] == MapCode.MY_POSITION) {
                    canvas.drawCircle(w * x + w / 2, h * i + h / 2, w / 2, myPosition);
                } else if (map[x][i] == MapCode.TARGET) {
                    canvas.drawCircle(w * x + w / 2, h * i + h / 2, w / 2, target);
                } else if (map[x][i] == MapCode.DESTINATION) {
                    canvas.drawCircle(w * x + w / 2, h * i + h / 2, w / 2, destination);
                } else if (map[x][i] == MapCode.BLOCKED) {
                    canvas.drawCircle(w * x + w / 2, h * i + h / 2, w / 2, blocked);
                }
            }
        }

    }

    public void setMap(int[][] map) {
        this.map = map;
    }
}
