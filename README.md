NXTplorer
=========

My robot for the course Robôs Móveis at FCUL. 

As the name implies, it explores an unknown terrain and makes a map of it.

It features an Android app that connects to the robot via bluetooth. It can receive and display the robot's map and interact with it to control the robot.

![IMG_0096.jpg](https://bitbucket.org/repo/xMdeon/images/1539907616-IMG_0096.jpg)
