import lejos.robotics.pathfinding.Path;

public class Main {
    public static void main(final String[] args) {

        final Point p1 = new Point(1,3);
        p1.f = 20;
        final Point p2 = new Point(1,3);
        p2.f = 40;
        final Point p3 = new Point(1,3);
        p3.f = 50;
        final Point p4 = new Point(1,3);
        p4.f = 70;
        final Point p5 = new Point(1,3);
        p5.f = 90;

        final PriorityQueue<Point> priorityQueue =  new PriorityQueue<Point>(new PointComparator());

        priorityQueue.add(p3);
        priorityQueue.add(p1);
        priorityQueue.add(p4);
        priorityQueue.remove(p4);
        priorityQueue.add(p4);
        priorityQueue.add(p5);
        priorityQueue.remove(p5);
        priorityQueue.add(p2);
        priorityQueue.add(p5);
        priorityQueue.remove(p1);

        System.out.println(priorityQueue);

        final Robot robot = new Robot();

        final int size = 10;
        final int[][] map = new int[size][size];

        for (int x = 0; x < map.length; x++) {
            for (int i = 0; i < map[x].length; i++) {
                map[x][i] = MapCode.UNKNOWN;
            }
        }

        map[4][0] = MapCode.BLOCKED;
        map[4][1] = MapCode.BLOCKED;
        map[4][2] = MapCode.BLOCKED;
        map[4][3] = MapCode.BLOCKED;
        map[4][4] = MapCode.BLOCKED;
        map[4][5] = MapCode.BLOCKED;
        map[4][6] = MapCode.BLOCKED;
        map[4][7] = MapCode.BLOCKED;

        robot.setMap(map);

        final Path path = robot.aStar(new Point(5,5));

        System.out.println(path);
    }
}

