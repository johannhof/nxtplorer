import java.util.*;

class PriorityQueue<E>{

    private final Comparator<E> comparator;
    private final List<E> internalList;

    PriorityQueue(final Comparator<E> comparator) {
        this.comparator = comparator;
        this.internalList = new LinkedList<E>();
    }

    public String toString() {
        return "PriorityQueue{" +
                "internalList=" + internalList +
                '}';
    }

    public int size() {
        return internalList.size();
    }

    public boolean isEmpty() {
        return internalList.isEmpty();
    }

    public boolean contains(Object o) {
        return internalList.contains(o);
    }

    public E poll() {
        return internalList.remove(0);
    }

    public boolean add(Object o) {
        if(internalList.contains((E) o)){
            return false;
        }
        for (int i = 0; i < internalList.size(); i++) {
            final Object item = internalList.get(i);
            if(comparator.compare((E) o, (E) item) <= 0){
                internalList.add(i,(E) o);
                return true;
            }
        }
        return internalList.add((E) o);
    }

    public boolean remove(Object o) {
        return internalList.remove(o);
    }

}