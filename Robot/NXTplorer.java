import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;


public class NXTplorer {

    public static void main(final String[] args) {

        final Robot robot = new Robot();

        robot.message("Waiting for connection");
        final BTConnection connection = Bluetooth.waitForConnection();
        connection.setIOMode(NXTConnection.RAW);

        final DataInputStream dataIn = new DataInputStream(connection.openInputStream());
        final DataOutputStream dataOut = new DataOutputStream(connection.openOutputStream());
        robot.setOutstream(dataOut);
        robot.message("Connected", true);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        final int instruction;
                        instruction = dataIn.readInt();
                        if (instruction == MessageCode.EXPLORE) {
                            robot.explore();
                        } else if (instruction == MessageCode.MAP) {
                            // receive new map and overwrite robot map
                            final int size = dataIn.readInt();
                            final int[][] map = new int[size][size];

                            for (int x = 0; x < map.length; x++) {
                                for (int i = 0; i < map[x].length; i++) {
                                    map[x][i] = dataIn.readInt();
                                }
                            }

                            robot.setMap(map);
                        } else if (instruction == MessageCode.MOVE) {
                            final int x = dataIn.readInt();
                            final int y = dataIn.readInt();
                            robot.goTo(new Point(x, y), false);
                        } else if (instruction == MessageCode.REPOSITION) {
                            final int x = dataIn.readInt();
                            final int y = dataIn.readInt();
                            robot.setPosition(x, y);
                        } else if (instruction == MessageCode.END) {
                            robot.message("Bye bye", true);
                            System.exit(0);
                            break;
                        }
                    } catch (final IOException e) {
                        e.printStackTrace();
                        break;
                    }
                }
            }
        }).run();

    }
}

