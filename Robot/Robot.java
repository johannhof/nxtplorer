import lejos.nxt.*;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.robotics.navigation.Navigator;
import lejos.robotics.navigation.Pose;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Robot {
    public static final int GRID_SIZE = 10;
    private final UltrasonicSensor bottom;
    private final UltrasonicSensor sonic;
    private int[][] map;
    private int x;
    private int y;
    private final DifferentialPilot pilot;
    private final Navigator navigator;
    private DataOutputStream outstream;
    private int heading;

    public void setOutstream(final DataOutputStream outstream) {
        this.outstream = outstream;
    }

    public List<Point> aStar(final Point dest) {
        final PriorityQueue<Point> open = new PriorityQueue<Point>(new PointComparator());
        final Set<Point> closed = new HashSet<Point>();

        final Point start = new Point(x, y);
        start.f = 0;
        start.g = 0;

        open.add(dest);

        while (!open.isEmpty()) {
            final Point cur = open.poll();

            if (cur.equals(start)) {
                final List<Point> path = new LinkedList<Point>();

                Point pre = cur.predecessor;
                while (pre != null) {
                    path.add(pre);
                    pre = pre.predecessor;
                }

                return path;
            }

            closed.add(cur);

            final Point[] successors = {
                    new Point(cur.x + 1, cur.y),
                    new Point(cur.x, cur.y + 1),
                    new Point(cur.x - 1, cur.y),
                    new Point(cur.x, cur.y - 1)
            };

            for (final Point successor : successors) {
                if (!inArray(successor.x, successor.y) ||
                        map[successor.x][successor.y] == MapCode.BLOCKED ||
                        closed.contains(successor)) {
                    continue;
                }

                final int tentative_g = cur.g + 1;

                if (open.contains(successor) && tentative_g >= successor.g) {
                    continue;
                }

                successor.g = tentative_g;
                successor.predecessor = cur;

                successor.f = tentative_g + h(successor, start);

                open.remove(successor);
                open.add(successor);
            }

        }


        return null;
    }

    private int h(final Point node, final Point dest) {
        return Math.abs(dest.x - node.x) + Math.abs(dest.y - node.y);
    }

    public void log(final String message) {
        try {
            outstream.writeInt(MessageCode.MESSAGE);
            outstream.writeUTF(message);
            outstream.flush();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    public void setPosition(final int x, final int y) {
        this.x = x;
        this.y = y;
        final Pose pose = navigator.getPoseProvider().getPose();
        navigator.getPoseProvider().setPose(new Pose(x * GRID_SIZE, y * GRID_SIZE, pose.getHeading()));

        log("Set new Position: " + x + "/" + y);
        sendMap(null, null);
    }

    public Robot() {
        this.x = 0;
        this.y = 0;

        heading = 0;
        bottom = new UltrasonicSensor(SensorPort.S1);
        sonic = new UltrasonicSensor(SensorPort.S2);
        pilot = new DifferentialPilot(5.6f, 12.2f, Motor.A, Motor.B);
        pilot.setTravelSpeed(15);
        pilot.setRotateSpeed(30);
        navigator = new Navigator(pilot);
    }

    public void setMapTile(final int i, final int val) {
        switch (relativePosition(i)) {
            case 1:
                if (inArray(x + 1, y)) {
                    map[x + 1][y] = val;
                }
            case 3:
                if (inArray(x, y + 1)) {
                    map[x][y + 1] = val;
                }
            case 5:
                if (inArray(x - 1, y)) {
                    map[x - 1][y] = val;
                }
            case 7:
                if (inArray(x, y - 1)) {
                    map[x][y - 1] = val;
                }
        }
    }

    private boolean inArray(final int x, final int y) {
        return x >= 0 && x < map.length && y >= 0 && y < map.length;
    }


    public int getMapTile(final int i) {
        switch (relativePosition(i)) {
            case 1:
                if (inArray(x + 1, y)) {
                    return map[x + 1][y];
                }
            case 3:
                if (inArray(x, y + 1)) {
                    return map[x][y + 1];
                }
            case 5:
                if (inArray(x - 1, y)) {
                    return map[x - 1][y];
                }
            case 7:
                if (inArray(x, y - 1)) {
                    return map[x][y - 1];
                }
        }
        return 3;
    }

    private int relativePosition(int i) {
        i += heading;

        if (i > 7) {
            i -= 8;
        }

        return i;
    }

    private Point getPoint(final int i) {
        switch (relativePosition(i)) {
            case 1:
                return new Point(x + 1, y);
            case 3:
                return new Point(x, y + 1);
            case 5:
                return new Point(x - 1, y);
            case 7:
                return new Point(x, y - 1);
        }
        return null;
    }


    public void checkNeighbors() {
        try {
            Motor.C.rotate(90);
            Thread.sleep(500);
            for (int i = 0; i < 10; i++) {
                if (sonic.getDistance() < GRID_SIZE) {
                    setMapTile(7, MapCode.BLOCKED);
                    break;
                } else {
                    log("Distance:" + sonic.getDistance());
                    setMapTile(7, MapCode.DISCOVERED);
                }
                Thread.sleep(50);
            }

            Motor.C.rotate(-180);
            Thread.sleep(500);
            for (int i = 0; i < 10; i++) {
                if (sonic.getDistance() < GRID_SIZE) {
                    setMapTile(3, MapCode.BLOCKED);
                    break;
                } else {
                    log("Distance:" + sonic.getDistance());
                    setMapTile(3, MapCode.DISCOVERED);
                }
                Thread.sleep(50);
            }

            Motor.C.rotate(90);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Point findNextDestination() {
        // TODO make more sophisticated

        // forward
        if (getMapTile(1) == MapCode.UNKNOWN) {
            final Point point = getPoint(1);
            if (point != null) {
                return point;
            }
        }

        // left
        if (getMapTile(3) == MapCode.UNKNOWN) {
            final Point point = getPoint(3);
            if (point != null) {
                headLeft();
                return point;
            }
        }

        // right
        if (getMapTile(7) == MapCode.UNKNOWN) {
            final Point point = getPoint(7);
            if (point != null) {
                headRight();
                return point;
            }
        }

        // back
        if (getMapTile(5) == MapCode.UNKNOWN) {
            final Point point = getPoint(5);
            if (point != null) {
                headBack();
                return point;
            }
        }

        for (int w = 0; w < map.length; w++) {
            if (map[w][y] == MapCode.UNKNOWN) {
                return new Point(w, y);
            }
        }

        for (int h = 0; h < map.length; h++) {
            if (map[x][h] == MapCode.UNKNOWN) {
                return new Point(x, h);
            }
        }

        for (int w = 0; w < map.length; w++) {
            for (int h = 0; h < map[w].length; h++) {
                if (map[w][h] == MapCode.UNKNOWN) {
                    return new Point(w, h);
                }
            }
        }

        return null;
    }

    private void headBack() {
        if (heading >= 4) {
            heading -= 4;
        } else {
            heading += 4;
        }
    }

    private void headRight() {
        if (heading == 6) {
            heading = 0;
        } else {
            heading += 2;
        }
    }

    private void headLeft() {
        if (heading == 0) {
            heading = 6;
        } else {
            heading -= 2;
        }
    }

    public void explore() {

        if (map == null) {
            return;
        }
        log("Starting to explore!");
        while (true) {

            final Point dest = findNextDestination();

            // nowhere to go
            if (dest == null) {
                log("I'm done! " + x + "/" + y);
                try {
                    outstream.writeInt(3); // END
                } catch (final IOException e) {
                    e.printStackTrace();
                }
                return;
            }

            if (!goTo(dest, true)) {
                return;
            }
        }
    }

    private int obstacleTries = 0;

    public void move(final Point dest, final boolean scout) throws ObstacleException {
        final Pose pose = navigator.getPoseProvider().getPose();
        log("Heading from (cm) " + pose.getX() + "/" + pose.getY() + " to " + dest.x * GRID_SIZE + "/" + dest.y + GRID_SIZE);
        navigator.goTo(dest.x * GRID_SIZE, dest.y * GRID_SIZE);
        while (navigator.isMoving()) {
            final int sonicDistance = sonic.getDistance();
            final int bottomDistance = bottom.getDistance();
            if (sonicDistance < GRID_SIZE || bottomDistance > 40) {
                log("Obstacle at " + sonicDistance + "/" + bottomDistance);
                navigator.stop();
                final ObstacleException.Type type;
                if (sonicDistance < 20) {
                    type = ObstacleException.Type.TOP;
                } else {
                    type = ObstacleException.Type.BOTTOM;
                }
                throw new ObstacleException(type);
            }
        }
        this.x = dest.x;
        this.y = dest.y;
        if (scout) {
            checkNeighbors();
        }
    }

    public void distance() {
        message(sonic.getDistance() + "/" + bottom.getDistance());
    }

    public int[][] getMap() {
        return map;
    }

    public void setMap(final int[][] map) {
        this.map = map;
    }

    public void message(final String text) {
        message(text, false);
    }

    public void message(final String text, final boolean important) {
        if (important) {
            Sound.beep();
        }
        LCD.clear();
        LCD.drawString(text, 0, 0);
    }

    public boolean goTo(final Point dest, final boolean scout) {
        do {
            // find path
            final List<Point> path = aStar(dest);

            // no path for destination found
            if (path == null) {
                map[dest.x][dest.y] = MapCode.BLOCKED;
                return true;
            }

            for (final Point next : path) {
                sendMap(next, dest);

                try {
                    move(next, scout);
                    // set point as discovered
                    this.map[next.x][next.y] = MapCode.DISCOVERED;
                    log("Cleared " + x + "/" + y);
                    obstacleTries = 0;
                } catch (final ObstacleException e) {
                    final Pose pose = navigator.getPoseProvider().getPose();
                    final float distance = pose.distanceTo(new lejos.geom.Point(x * GRID_SIZE, y * GRID_SIZE));
                    pilot.travel(-distance);
                    navigator.getPoseProvider().setPose(new Pose(x * GRID_SIZE, y * GRID_SIZE, pose.getHeading()));
                    navigator.clearPath();
                    // set point as blocked
                    this.map[next.x][next.y] = MapCode.BLOCKED;

                    if (obstacleTries++ > 2) {
                        log("I'm giving up");
                        return false;
                    }
                    break;
                }
                if(next.equals(dest)){
                    return true;
                }
            }
        } while (!scout);
        return true;
    }

    private void sendMap(final Point next, final Point dest) {
        try {
            map[x][y] = MapCode.MY_POSITION;
            if (next != null) {
                map[next.x][next.y] = MapCode.TARGET;
            }
            if (dest != null) {
                map[dest.x][dest.y] = MapCode.DESTINATION;
            }
            outstream.writeInt(MessageCode.MAP);
            outstream.writeInt(map.length);
            for (final int[] ints : map) {
                for (final int i : ints) {
                    outstream.writeInt(i);
                }
            }
            outstream.flush();
            map[x][y] = MapCode.DISCOVERED;
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}
