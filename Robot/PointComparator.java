class PointComparator implements Comparator<Point> {
    @Override
    public int compare(final Point o1, final Point o2) {
        return o1.f - o2.f;
    }
}
