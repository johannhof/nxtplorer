public class ObstacleException extends Exception{
    private static final long serialVersionUID = 4640616590813111660L;
    private final Type type;

    public Type getType() {
        return type;
    }

    public enum Type{ TOP, BOTTOM }

    public ObstacleException(Type type){
        this.type = type;
    }
}
